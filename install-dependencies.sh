#!/bin/sh

sudo apt update && sudo apt upgrade -y
sudo apt install -y python3-pip libopenjp2-7 libtiff5 python3-numpy
sudo pip3 install -r requirements.txt
