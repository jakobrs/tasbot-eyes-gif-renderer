from PIL import Image, ImageSequence
import time
import board
import neopixel
import signal
import sys
import argparse

pixels = None
original_sigint = None

def loop_gif(brightness, input_file, x_offset, y_offset):
    # This is physical pin 12 and GPIO18
    pixel_pin = board.D18

    # The number of leds in the eyes
    num_pixels = 154

    # The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
    # For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
    ORDER = neopixel.GRB

    global pixels

    pixels = neopixel.NeoPixel(
        pixel_pin, num_pixels, brightness=brightness, auto_write=False, pixel_order=ORDER
    )

    positionArray = [
        ['x','x',0,1,2,3,'x','x','x','x',101,100,99,98,97,96,95,94,'x','x','x','x',105,104,103,102,'x','x','x','x'],
        ['x',4,5,6,7,8,9,'x','x',84,85,86,87,88,89,90,91,92,93,'x','x',111,110,109,108,107,106,'x','x','x'],
        [10,11,12,13,14,15,16,17,'x','x','x','x','x','x','x','x','x','x','x','x',119,118,117,116,115,114,113,112,'x','x'],
        [18,19,20,21,22,23,24,25,'x','x','x',83,82,81,80,79,78,'x','x','x',127,126,125,124,123,122,121,120,'x','x'],
        [26,27,28,29,30,31,32,33,'x','x',70,71,72,73,74,75,76,77,'x','x',135,134,133,132,131,130,129,128,'x','x'],
        [34,35,36,37,38,39,40,41,'x','x','x','x','x','x','x','x','x','x','x','x',143,142,141,140,139,138,137,136,'x','x'],
        ['x',42,43,44,45,46,47,'x','x','x',68,67,66,65,64,63,62,61,'x','x','x',149,148,147,146,145,144,'x','x','x'],
        ['x','x',48,49,50,51,'x','x','x',69,52,53,54,55,56,57,58,59,60,'x','x','x',153,152,151,150,'x','x','x','x'],
    
    ]
    # Standard gif for demonstrating offset
    # im = Image.open("gifs/battlebots.gif")
    im = Image.open(input_file)


    #  Offset visualized:
    #   ____________________________________________
    #  |              ^                             |
    #  |              |                             |
    #  |              | y-offset                    |
    #  |              |                             |
    #  |   x-offset   v_________________            |
    #  |<----------->|        eye       |           |
    #  |             |   display_area   |           |
    #  |             |_________________ |           |
    #  |                                            |
    #  |                                            |
    #  |                                            |
    #  |                                            |
    #  |____________________________________________|
    


    x_offset = x_offset
    y_offset = y_offset
    while True:
        for frame in ImageSequence.Iterator(im):
            # print (frame.info)
            duration = frame.info['duration']
            time_end  = time.time() + (duration/1000)
            while time.time() < time_end:
                cur_frame = frame.convert('RGBA')

                for x in range(x_offset, x_offset + 28):
                    
                    for y in range(y_offset, y_offset + 8):
                        
                        r, g, b, a = cur_frame.getpixel((x,y))

                        if positionArray[y-y_offset][x-x_offset] != 'x':
                            if a > 0:
                                pixels[positionArray[y-y_offset][x-x_offset]] = (r,g,b)
                            else:
                                pixels[positionArray[y-y_offset][x-x_offset]] = (0,0,0)
                
                pixels.show()

def exit_gracefully(signum, frame):
    # restore the original signal handler as otherwise evil things will happen
    # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
    signal.signal(signal.SIGINT, original_sigint)
    pixels.deinit()
    sys.exit(1)

    # restore the exit gracefully handler here    
    signal.signal(signal.SIGINT, exit_gracefully)

    loop_gif(brightness, input_file, x_offset, y_offset)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Control TASBot\'s eyes by supplying a gif!')
    parser.add_argument("brightness",  type = float)
    parser.add_argument("x_offset", type = int)
    parser.add_argument("y_offset", type = int)
    parser.add_argument("input_file", type = str)

    args = parser.parse_args()

    # store the original SIGINT handler
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)

    loop_gif(args.brightness, args.input_file, args.x_offset, args.y_offset)



    
